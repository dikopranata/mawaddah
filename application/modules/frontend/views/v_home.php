<!-- __________________________________________________ Start Top -->

				<section id="top">
					<div class="top_inner">
						<div class="wrap_rev_slider">
							<!-- START REVOLUTION SLIDER 2.3.1 -->
							<div id="rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
								<div id="rev_slider" class="rev_slider fullwidthabanner" style="display:none; max-height:479px; height:479;">						
									<ul>
										<li data-transition="random" data-slotamount="7" data-masterspeed="300" >
											<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/bg-1.jpg" />
											<div data-linktoslide="4" data-easing="easeOutExpo" data-start="2900" data-speed="1000" data-y="283" data-x="671" class="caption sft tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-2.jpg" />
											</div>
											<div class="caption randomrotate tp-caption start" data-easing="easeOutExpo" data-start="2200" data-speed="700" data-y="283" data-x="883">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-3.jpg" />
											</div>
											<div data-easing="easeOutBack" data-start="2000" data-speed="800" data-y="64" data-x="671" class="caption lfb tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-1.jpg" />
											</div>
										</li>
										<li data-transition="random" data-slotamount="7" >
											<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/bg-3.jpg" />
											<div class="tp-caption fade" data-easing="easeOutExpo" data-start="2000" data-speed="700" data-y="395" data-x="61">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-shadow2.png" />
											</div>
											<div data-linktoslide="4" data-easing="easeOutExpo" data-start="1500" data-speed="1000" data-y="427" data-x="85" class="caption lfb">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-shadow.png" />
											</div>
											<div class="caption lft" data-easing="easeOutExpo" data-start="1300" data-speed="1000" data-y="50" data-x="71">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-10.png" />
											</div>
											<div class="tp-caption lft" data-x="134" data-y="94" data-speed="1000" data-start="1300" data-easing="easeOutExpo"  ><iframe src="https://www.youtube.com/embed/1KpFm-KV5Ek" width="395" height="295" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
												
											</div>
											<div data-easing="easeOutBack" data-start="1800" data-speed="700" class="caption lfb tp-caption start" data-y="87" data-x="706">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-7.jpg" />
											</div>
											<div data-easing="easeOutBack" data-start="2200" data-speed="800" data-y="285" data-x="706" class="caption lfb tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-8.jpg" />
											</div>
                                            <div data-easing="easeOutBack" data-start="2400" data-speed="800" data-y="285" data-x="887" class="caption lfb tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-9.jpg" />
											</div>
										</li>
										<li data-transition="random" data-slotamount="7" >
											<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/bg-2.jpg" />
                                            <div data-linktoslide="4" data-easing="easeOutExpo" data-start="1800" data-speed="1000" data-y="82" data-x="76" class="caption sft tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-4.jpg" />
											</div>
											<div class="caption randomrotate tp-caption start" data-easing="easeOutExpo" data-start="1800" data-speed="700" data-y="82" data-x="405">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-5.jpg" />
											</div>
											<div data-easing="easeOutBack" data-start="2200" data-speed="800" data-y="245" data-x="405" class="caption lfb tp-caption start">
												<img alt="" src="<?php echo base_url()?>assets/frontend/images/revolution/img-6.jpg" />
											</div>
										</li>
									</ul>
                                </div>
							</div>
							<script type="text/javascript">
    
                                var tpj=jQuery;
                                
                                                    tpj.noConflict();
                                                
                                var revapi2;
                                
                                tpj(document).ready(function() {
                                
                                if (tpj.fn.cssOriginal != undefined)
                                    tpj.fn.css = tpj.fn.cssOriginal;
                                
                                if(tpj('#rev_slider').revolution == undefined)
                                    revslider_showDoubleJqueryError('#rev_slider');
                                else
                                   revapi2 = tpj('#rev_slider').show().revolution(
                                    {	
                                        delay:9000,
                                        startwidth:1160,
                                        startheight:479,
                                        hideThumbs:200,
                                        
                                        thumbWidth:100,
                                        thumbHeight:50,
                                        thumbAmount:5,
                                        
                                        navigationType:"bullet",
                                        navigationArrows:"solo",
                                        navigationStyle:"round",
                                        
                                        touchenabled:"on",
                                        onHoverStop:"on",
                                        
                                        navigationHAlign:"center",
                                        navigationVAlign:"bottom",
                                        navigationHOffset:0,
                                        navigationVOffset:21,
    
                                        soloArrowLeftHalign:"left",
                                        soloArrowLeftValign:"center",
                                        soloArrowLeftHOffset:0,
                                        soloArrowLeftVOffset:0,
    
                                        soloArrowRightHalign:"right",
                                        soloArrowRightValign:"center",
                                        soloArrowRightHOffset:0,
                                        soloArrowRightVOffset:0,
                                                
                                        shadow:1,
                                        fullWidth:"on",
    
                                        stopLoop:"off",
                                        stopAfterLoops:-1,
                                        stopAtSlide:-1,
    
                                        shuffle:"off",
                                        
                                        hideSliderAtLimit:0,
                                        hideCaptionAtLimit:0,
                                        hideAllCaptionAtLilmit:0,
                                        startWithSlide:0	
                                    });
                                
                                });	//ready
                                
                            </script>
							<div class="cl"></div>
						</div>
					</div>
				</section>
<!-- __________________________________________________ Finish Top -->	

<!-- __________________________________________________ Start Middle -->
			
				<section id="middle">
					<div class="middle_inner">

<!-- __________________________________________________ Start Content -->

						<section id="middle_content">
							<div class="entry">
								<div class="one_third aligncenter">
									<div class="featured_block">
										<div class="cmsms_services_icon">
											<img alt="" class="aligncenter" src="<?php echo base_url()?>assets/frontend/images/img/home_1.png">
										</div>
										<h2>Responsive Design</h2>
										<p>Increase is guaranteed to have a great appearance on all sorts of devices, on large desktop screens as well as on tablets and mobiles. </p>
									</div>
								</div>
								<div class="one_third aligncenter">
									<div class="featured_block">
										<div class="cmsms_services_icon">
											<img alt="" class="aligncenter" src="<?php echo base_url()?>assets/frontend/images/img/home_2.png">
										</div>
										<h2>Easy to Customize</h2>
										<p>Increase template offers super flexibility for color editing and easily turns from a minimalistic style template into a juicy website with great animation effects.</p>
									</div>
								</div>
								<div class="one_third last aligncenter">
									<div class="featured_block">
										<div class="cmsms_services_icon">
											<img alt="" class="aligncenter" src="<?php echo base_url()?>assets/frontend/images/img/home_3.png">
										</div>
										<h2>Advanced SEO</h2>
										<p>Increase is a  SEO-friendly HTML template. In features super-clean Css3  and HTML5 that  search engines will love, successfully crawl and index.</p>
									</div>
								</div>
								<div class="cl"></div>
								<div class="one_first">
									<aside class="widget widget_text">
										<div class="textwidget">
											<section class="post_type_shortcode four_blocks">
												<h3 class="cms_title">Latest Works</h3>
												<script type="text/javascript">
													jQuery(document).ready(function () { 
														jQuery('.post_type_list').cmsmsResponsiveContentSlider( { 
															sliderWidth : '100%', 
															sliderHeight : 'auto', 
															animationSpeed : 500, 
															animationEffect : 'slide', 
															animationEasing : 'easeInOutExpo', 
															pauseTime : 0, 
															activeSlide : 1, 
															touchControls : false, 
															pauseOnHover : false, 
															arrowNavigation : true, 
															slidesNavigation : false 
														} );
													} );
												</script>
												<ul class="post_type_list responsiveContentSlider">
													<li class="latest_item">
														<article class="portfolio format-album">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_1.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#6cc437;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Lifestyle</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Design</a>, </li>
																			<li><a href="#">Corporate</a><li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<span class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</span>
																</div>
															</div>
														</article>
														<article class="portfolio format-slider">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_3.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#fabe09;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Half The World Away</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Design</a>, </li>
																			<li><a href="#">Corporate</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</div>
																</div>
															</div>
														</article>
														<article class="portfolio format-album">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_2.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#6cc437;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Going Forward</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Business</a>, </li>
																			<li><a href="#">Corporate</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet cons ectetuer adipiscing elit praesent vestibulu moles lacus aenean ummy hendrerit mahas. Praesent posuere tortor sed mi ullamcorper sodales. Cras porta purus in augue molestie aliquet. Aenean orci ipsum, ornare sit amet imper</p>
																	</div>
																</div>
															</div>
														</article>
														<article class="portfolio format-video">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_4.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#f97a14;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Underneath The Sky</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Photography</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</div>
																</div>
															</div>
														</article>
													</li>
													<li class="latest_item">
														<article class="portfolio format-slider">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_3.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#fabe09;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Half The World Away</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Design</a>, </li>
																			<li><a href="#">Corporate</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</div>
																</div>
															</div>
														</article>
														<article class="portfolio format-album">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_2.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#6cc437;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Going Nowhere</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Business</a>, </li>
																			<li><a href="#">Corporate</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet cons ectetuer adipiscing elit praesent vestibulu moles lacus aenean ummy hendrerit mahas. Praesent posuere tortor sed mi ullamcorper sodales. Cras porta purus in augue molestie aliquet. Aenean orci ipsum, ornare sit amet imper</p>
																	</div>
																</div>
															</div>
														</article>
														<article class="portfolio format-video">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_4.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#f97a14;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Underneath The Sky</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Photography</a></li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<div class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</div>
																</div>
															</div>
														</article>
														<article class="portfolio format-album">
															<div class="portfolio_inner">
																<a href="album_four.html"></a>
																<figure>
																	<img src="<?php echo base_url()?>assets/frontend/images/img/fproject_4_1.jpg" alt="" class="fullwidth" />
																</figure>
																<div class="project_rollover" style="background-color:#6cc437;">
																	<header class="entry-header">
																		<h6 class="entry-title"><a href="album_four.html">Lifestyle</a></h6>
																		<ul class="cmsms_category">
																			<li><a href="#">Design</a>, </li>
																			<li><a href="#">Corporate</a><li>
																		</ul>
																		<span class="cmsms_post_img"></span>
																	</header>
																	<span class="entry-content">
																		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis arcu vitae magna egestas pulvinar vitae</p>
																	</span>
																</div>
															</div>
														</article>
													</li>
												</ul>
											</section>
										</div>
									</aside>
								</div>
								<div class="cl"></div>
								<div class="two_third">
									<div class="tour_content">
									<h2>Our Services</h2><br />
										<ul class="tour">
											<li class="current">
												<a href="#">
													Responsive &amp; Retina
												</a>
											</li>
											<li>
												<a href="#">
													Valid Code
												</a>
											</li>
											<li>
												<a href="#">
													Great Design
												</a>
											</li>
											<li>
												<a href="#">
													Helpful Support
												</a>
											</li>
											<li>
												<a href="#">
													Detailed Docs
												</a>
											</li>
										</ul>
										<div class="tour_box" style="display:block;">
											<div class="tour_box_inner">
												<h5>Responsive &amp; Retina</h5>
												<p>Choosing Increase HTML will be one of the most successful solutions of yours. In was initially created to help you make a really good website that will bring you to the top, whatever your sphere is.  Increase wordpress theme features minimal but juicy design that will be friendly to all possible customization you might want to apply. Increase template offers super flexibility for color editing and easily turns from a minimalistic style template into a juicy website with great animation effects. Increase is a  SEO-friendly HTML template.</p>
											</div>
										</div>
										<div class="tour_box">
											<div class="tour_box_inner">
												<h5>Valid Code</h5>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.Vestibulum vulputate aliquam risus. Praesent consequat scelerisque velit eu elementum. Praesent posuere tortor sed mi ullamcorper sodales. Cras porta purus in. <a href="#">Lorem ipsum dolor</a> sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et.</p>
											</div>
										</div>
										<div class="tour_box">
											<div class="tour_box_inner">
												<h5>Great Design</h5>
												<p>Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum vulputate aliquam risus. Praesent consequat scelerisque velit eu elementum.</p>
												<p>Cras porta purus in. <a href="#">Lorem ipsum dolor</a> sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et.</p>
											</div>
										</div>
										<div class="tour_box">
											<div class="tour_box_inner">
												<h5>Helpful Support</h5>
												<p>Mauris dictum aliquam nisi, vitae elementum magna rhoncus et. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
												<p>Vestibulum vulputate aliquam risus. Praesent consequat scelerisque velit eu elementum. Praesent posuere tortor sed mi ullamcorper sodales. Cras porta purus in. Lorem ipsum dolor  sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et.</p>
											</div>
										</div>
										<div class="tour_box">
											<div class="tour_box_inner">
												<h5>Detailed Docs</h5>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum vulputate aliquam risus. Praesent consequat scelerisque velit eu elementum. Praesent posuere tortor sed mi ullamcorper sodales. Cras porta purus in. <a href="#">Lorem ipsum dolor</a> sit amet, consectetur adipiscing elit. Aenean ut justo nec arcu molestie molestie non quis magna. Mauris dictum aliquam nisi, vitae elementum magna rhoncus et.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="one_third last">
									<div class="cmsms_shortcode_testimonials">
										<h2>Testimonials</h2><br />
										<header class="entry-header">
											<blockquote>
												<h6>Never expected to get such a powerful-functionality theme for this little money! It is a real pleasure to work with, I think it simply guesses my thoughts! </h6>
											</blockquote>
										</header>
										<div class="entry-content">
											<div class="alignleft">
												<figure>
													<a href="openpost.html" title="Standard blogpost with image">
														<img src="<?php echo base_url()?>assets/frontend/images/img/home_4.jpg">
													</a>
												</figure>
											</div>
											<p>John Parker</p>
											<ul class="cmsms_tags">
												<li><a href="openpost.html">Businessman</a>, </li>
												<li><a href="openpost.html">Colorado</a></li>
											</ul>
										</div>	
									</div>
								</div>
								<div class="cl"></div>
								
								<div class="colored_shortcode">
									<div class="colored_title">
										<div class="colored_title_inner">
											<h2>When vested in power the beauty always wins</h2>
											<p>Increase is a wordpress theme that easily adapts to your needs, allowing you quickly create a beautiful website</p>
										</div>
									</div>
									<a class="colored_button" href="#"><span class="icon_banner icon_down"></span>Buy Now!</a>
								</div>
							</div>
						</section>
<!-- __________________________________________________ Finish Content -->

					</div>
				</section>
<!-- __________________________________________________ Finish Middle -->