<?php 

class Template extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		
	}

	function member_template($data = null)
	{
		
		$this->load->view('Template/v_member_template',$data);
	}

	function login_register_template($data = null)
	{
		
		$this->load->view('Template/v_login_register_template',$data);
	}

	function frontend_template($data = null)
	{
		
		$this->load->view('Template/v_frontend_template',$data);
	}

	
}