<!DOCTYPE html>
<!--[if lt IE 7]><html dir="ltr" lang="en-US" class="ie6"><![endif]-->
<!--[if IE 7]><html dir="ltr" lang="en-US" class="ie7"><![endif]-->
<!--[if IE 8]><html dir="ltr" lang="en-US" class="ie8"><![endif]-->
<!--[if gt IE 8]><!--><html dir="ltr" lang="en-US"><!--<![endif]-->
<head>
	<meta content="text/html;charset=utf-8" http-equiv="content-type" />
	<meta name="description" content="cmsmasters responsive html5 website template" />
	<meta name="keywords" content="html5, css3, template, responsive, adaptive" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontend/images/favicon.png" type="image/x-icon" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/styles/fonts.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" type="text/css" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oxygen:400,300,700" type="text/css" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Headland+One" type="text/css" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/styles/jackbox.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/revolution/css/captions-original.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/revolution/css/captions.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/frontend/revolution/css/settings.css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url()?>assets/frontend/css/styles/jquery.jPlayer.css" type="text/css" media="screen" />
	<script src="<?php echo base_url()?>assets/frontend/js/modernizr.custom.all.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/frontend/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url()?>assets/frontend/js/jquery.easing.js" type="text/javascript"></script>
	<script type="<?php echo base_url()?>assets/frontend/text/javascript" src="<?php echo base_url()?>assets/frontend/revolution/js/jquery.themepunch.revolution.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/revolution/js/jquery.themepunch.revolution.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url()?>assets/frontend/revolution/js/jquery.themepunch.plugins.min.js"></script>
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="css/styles/ie.css" type="text/css" />
			<link rel="stylesheet" href="css/styles/ie_css3.css" type="text/css" media="screen" />
			<![endif]-->
			<title>Increase - Responsive HTML5 Site Template</title>	
		</head>
		<body>
			<!-- __________________________________________________ Start Page -->

			<section id="page">
				<a href="#" id="slide_top"></a>
				<!-- __________________________________________________ Start Header -->

				<header id="header">
					<div class="header_inner">
						<div class="social_block">
							<ul>
								<li>
									<a href="#" target="_blank">
										<img alt="twitter" src="<?php echo base_url()?>assets/frontend/images/socicons/twitter.png">
									</a>
								</li>
								<li>
									<a href="#" target="_blank">
										<img alt="linkedin" src="<?php echo base_url()?>assets/frontend/images/socicons/linkedin.png">
									</a>
								</li>
								<li>
									<a href="#" target="_blank">
										<img alt="facebook" src="<?php echo base_url()?>assets/frontend/images/socicons/facebook.png">
									</a>
								</li>
								<li>
									<a href="#" target="_blank">
										<img alt="vimeo" src="<?php echo base_url()?>assets/frontend/images/socicons/vimeo.png">
									</a>
								</li>
								<li>
									<a href="#" target="_blank">
										<img alt="you tube" src="<?php echo base_url()?>assets/frontend/images/socicons/youtube.png">
									</a>
								</li>
							</ul>
							<a class="social_toggle" href="#"><span></span></a>
						</div>
						<a class="logo" href="index.html"><img src="<?php echo base_url()?>assets/frontend/images/logo.png" width="190" height="69" alt="" /></a>
						<a class="resp_navigation" href="javascript:void(0);"><span></span></a>
						<div class="cl"></div>
						<nav>
							<ul id="navigation">
								<li class="current_page_item"><a href="<?php echo base_url() ?>"><span>Home</span></a></li>

								
								<li><a href="<?php echo base_url('register') ?>"><span>Register</span></a></li>

								<li><a href="<?php echo base_url('login') ?>"><span>Login</span></a></li>
							</ul>
							<div class="cl"></div>
						</nav>
						<div class="cl"></div>
					</div>
				</header>
				<!-- __________________________________________________ Finish Header -->

				<div class="container">

					<?php $this->load->view($content);?>
					<!-- __________________________________________________ Start Bottom -->

					<section id="bottom">
						<div class="bottom_inner">
							<div class="one_fourth">
								<aside class="widget widget_custom_portfolio_entries">
									<script type="text/javascript">
										jQuery(document).ready(function () { 
											jQuery('.widget_custom_portfolio_entries_slides').cmsmsResponsiveContentSlider( { 
												sliderWidth : '100%', 
												sliderHeight : 'auto', 
												animationSpeed : 500, 
												animationEffect : 'slide', 
												animationEasing : 'easeInOutExpo', 
												pauseTime : 5000, 
												activeSlide : 1, 
												touchControls : true, 
												pauseOnHover : false, 
												arrowNavigation : true, 
												slidesNavigation : false 
											} );
										} );
									</script>
									<div class="widget_custom_portfolio_entries_container">
										<h2 class="widgettitle">Sliding article</h2>
										<ul class="widget_custom_portfolio_entries_slides responsiveContentSlider">
											<li>
												<figure>
													<img src="images/img/sliding_1.jpg" alt="" class="fullwidth" />
												</figure>
												<a href="#" class="widget_portfolio_link">Making design with magic</a>
												<p>Pellentesque tristique volutpat nunc, id rhoncus augue tristique </p>
											</li>
											<li>
												<figure>
													<img src="images/img/sliding_2.jpg" alt="" class="fullwidth" />
												</figure>
												<a href="#" class="widget_portfolio_link">Making design with magic</a>
												<p>Pellentesque tristique volutpat nunc, id rhoncus augue tristique </p>
											</li>
											<li>
												<figure>
													<img src="images/img/sliding_3.jpg" alt="" class="fullwidth" />
												</figure>
												<a href="#" class="widget_portfolio_link">Making design with magic</a>
												<p>Pellentesque tristique volutpat nunc, id rhoncus augue tristique </p>
											</li>
											<li>
												<figure>
													<img src="images/img/sliding_4.jpg" alt="" class="fullwidth" />
												</figure>
												<a href="#" class="widget_portfolio_link">Making design with magic</a>
												<p>Pellentesque tristique volutpat nunc, id rhoncus augue tristique </p>
											</li>
										</ul>
									</div>
								</aside>
							</div>
							<div class="one_fourth">
								<aside id="custom-twitter-4" class="widget widget_custom_twitter_entries">
									<script type="text/javascript">
										jQuery(document).ready(function () { 
											jQuery('#custom-twitter-4_tweets').jTweetsAnywhere( { 
												username : 'cmsmasters', 
												count : 2, 
												showTweetFeed : { 
													showTwitterBird : false, 
													showGeoLocation : false, 
													showInReplyTo : false, 
													includeRetweets : false 
												} 
											} ); 
										} );
									</script>
									<h2 class="widgettitle">Twitter</h2>
									<div id="custom-twitter-4_tweets"></div>
								</aside>
							</div>
							<div class="one_fourth">
								<aside class="widget widget_links">
									<h2 class="widgettitle">List widget</h2>
									<ul>
										<li><a href="#">Styling options</a></li>
										<li><a href="#">Theme options</a></li>
										<li><a href="#">Sidebars &amp; widgets</a></li>
										<li><a href="#">Shortcodes</a></li>
										<li><a href="#">Blogs &amp; posts</a></li>
										<li><a href="#">Sliders &amp; slides</a></li>
										<li><a href="#">Contact form builder</a></li>
									</ul>
								</aside>
							</div>
							<div class="one_fourth">
								<aside class="widget widget_custom_contact_form_entries">
									<h2 class="widgettitle">Quick contacts</h2>
									<div class="cmsms-form-builder">
										<div class="widgetinfo">Thank you! <br />Your message has been sent successfully.</div>
										<script type="text/javascript">
											jQuery(document).ready(function () { 
												jQuery('#form_contact_form_widget_001').validationEngine('init');

												jQuery('#form_contact_form_widget_001 a#contact_form_widget_001_wformsend').click(function () { 
													var form_builder_url = jQuery('#contact_form_widget_001_wurl').val();

													jQuery('#form_contact_form_widget_001 .loading').animate( {
														opacity : 1
													} , 250);

													if (jQuery('#form_contact_form_widget_001').validationEngine('validate')) { 
														jQuery.post(form_builder_url, {
															field_003 : jQuery('#field_003').val(), 
															field_004 : jQuery('#field_004').val(), 
															formname : 'contact_form_widget_001', 
															formtype : 'widget' 
														}, function () { 
															jQuery('#form_contact_form_widget_001 .loading').animate( { opacity : 0 }, 250);
															document.getElementById('form_contact_form_widget_001').reset();
															jQuery('#form_contact_form_widget_001').parent().find('.widgetinfo').hide();
															jQuery('#form_contact_form_widget_001').parent().find('.widgetinfo').fadeIn('fast');
															jQuery('html, body').animate( { scrollTop : (jQuery('#form_contact_form_widget_001').offset().top - 100) }, 'slow');
															jQuery('#form_contact_form_widget_001').parent().find('.widgetinfo').delay(5000).fadeOut(1000);
														} );

														return false;
													} else { 
														jQuery('#form_contact_form_widget_001 .loading').animate( { opacity : 0 }, 250);

														return false;
													}
												} );
											} );
										</script>
										<form action="#" method="post" id="form_contact_form_widget_001">
											<div class="form_info cmsms_input">
												<label for="field_003">Email *</label>
												<input type="text" name="wemail" id="field_003" size="22" tabindex="11" class="validate[required,custom[email]]" />
											</div>
											<div class="form_info cmsms_textarea">
												<label for="field_004">Message *</label>
												<textarea name="wmessage" id="field_004" cols="28" rows="6" tabindex="13" class="validate[required,minSize[3]]"></textarea>
											</div>
											<div class="loading"></div>
											<div><input type="hidden" name="contact_form_widget_001_wurl" id="contact_form_widget_001_wurl" value="http://magique-html.cmsmasters.net/php/sendmail.php" /></div><!-- Here you need to set the path to the sendmail file -->
											<div><a href="#" id="contact_form_widget_001_wformsend" class="button" tabindex="14"><span>Submit</span></a></div>
										</form>
									</div>
								</aside>
							</div>
						</div>
					</section>		
					<!-- __________________________________________________ Finish Bottom -->

				</div>
			</section>
			<!-- __________________________________________________ Finish Page -->


			<!-- __________________________________________________ Start Footer -->

			<footer id="footer">
				<div class="footer_inner">
					<span>Increase &copy; 2013 All rights reserved</span>
					<div class="fr">
						<ul id="footer_nav" class="footer_nav">
							<li><a href="#">Home</a></li>
							<li><a href="#">Features</a></li>
							<li><a href="#">Portfolio</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Contacts</a></li>
						</ul>
					</div>
				</div>
			</footer>
			<!-- __________________________________________________ Finish Footer -->

			<script src="<?php echo base_url()?>assets/frontend/js/jackbox.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/jackbox-lib.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/jquery.script.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/jquery.validationEngine.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/jquery.validationEngine-lang.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/jquery.jtweetsanywhere.js" type="text/javascript"></script>
			<script src="<?php echo base_url()?>assets/frontend/js/respond.js" type="text/javascript"></script>
		</body>
		</html>