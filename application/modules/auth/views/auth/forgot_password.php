<div class="col-sm-3"></div>
<div class="col-sm-6">

	<div class="form-box">
		<div class="form-top">
			<div class="form-top-left">
				<h3><?php echo lang('forgot_password_heading');?></h3>
				<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
			</div>
			<div class="form-top-right">
				<i class="fa fa-key"></i>
			</div>
		</div>
		<div class="form-bottom">
			<?php if(isset($message)) : ?>
				<div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
			<?php endif ?>

			
			<?php echo form_open("auth/forgot_password",array('role' => 'form-action','class' => "login-form"));?>

			<div class="form-group">
				<!-- <?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?> -->

				<?php echo form_input(array_merge($identity,array('class'=>'form-username form-control','id' => 'form-username','placeholder'=>'Email...')));?>
</div>

			<button type="submit" class="btn"><?php echo lang('forgot_password_submit_btn')?>!</button>
		</div></div>
	</div>
	<div class="col-sm-3"></div>