
<div class="container">      
  <section id="middle">
    <div class="middle_inner">
      <!-- __________________________________________________ Start Content -->
      <h1><?php echo lang('create_user_heading');?></h1>
      <p><?php echo lang('create_user_subheading');?></p>

      <?php if(isset($message)) : ?>
        <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
      <?php endif; ?>

      <?php echo form_open_multipart("register");?>

      <p>
        <?php echo lang('create_user_fname_label', 'first_name');?> <br />
        <?php echo form_input($first_name);?>
      </p>

      <p>
        <?php echo lang('create_user_lname_label', 'last_name');?> <br />
        <?php echo form_input($last_name);?>
      </p>
      
      <?php
      if($identity_column!=='email') {
        echo '<p>';
        echo lang('create_user_identity_label', 'identity');
        echo '<br />';
        echo form_error('identity');
        echo form_input($identity);
        echo '</p>';
      }
      ?>

      <p>
        <?php echo lang('create_user_company_label', 'company');?> <br />
        <?php echo form_input($company);?>
      </p>

      <p>
        <?php echo lang('create_user_email_label', 'email');?> <br />
        <?php echo form_input($email);?>
      </p>

      <p>
        <?php echo lang('create_user_phone_label', 'phone');?> <br />
        <?php echo form_input($phone);?>
      </p>
      <p>
        <?php echo lang('create_user_religi_label', 'religi');?> <br />
        <?php echo form_input($religi);?>
      </p>

      <p>
        <?php echo lang('create_user_manhaj_label', 'manhaj');?> <br />
        <?php echo form_input($manhaj);?>
      </p>
       <p>
        <?php echo lang('create_user_child_for_label', 'child_for');?> <br />
        <?php echo form_dropdown($child_for['name'],$child_for['data'],$child_for['value']);?>
      </p>
      <p>
        <?php echo lang('create_user_address_now_label', 'address_now');?> <br />
        <?php echo form_input($address_now);?>
      </p>
      <p>
        <?php echo lang('create_user_address_origin_label', 'address_origin');?> <br />
        <?php echo form_input($address_origin);?>
      </p>
      <p>
        <?php echo lang('create_user_wedding_blessing_label', 'wedding_blessing');?> <br />
        <?php echo form_dropdown($wedding_blessing['name'],$wedding_blessing['data'],$wedding_blessing['value']);?>
      </p>
      <p>
        <?php echo lang('create_user_date_of_place_label', 'date_of_place');?> <br />
        <?php echo form_input($date_of_place);?>
      </p>


      <p>
        <?php echo lang('create_user_birthday_label', 'birthday');?> <br />
        <?php echo form_dropdown($day['name'],$day['data'],$day['value']);?>
        <?php echo form_dropdown($month['name'],$month['data'],$month['value']);?>
        <?php echo form_dropdown($year['name'],$year['data'],$year['value']);?>
      </p>

      <p>
        <?php echo lang('create_user_gender_label', 'gender');?> <br />
        <?php echo form_dropdown($gender['name'],array('' => 'Choice One','0' => 'Male','1' => 'Female'),$gender['value']);?>
      </p>
      <p>
        <?php echo lang('create_user_merried_label', 'merried');?> <br />

        <?php echo form_dropdown($merried['name'],array('' => 'Choice One','0' => 'Single','1' => 'Merried','2' => 'Duda / Janda (Cerai)','3'=>'Duda / Janda (Meninggal)'),$merried['value']);?>
      </p>

      



    <p>
      <?php echo lang('create_user_password_label', 'password');?> <br />
      <?php echo form_input($password);?>
    </p>

    <p>
      <?php echo lang('create_user_password_confirm_label', 'password_confirm');?> <br />
      <?php echo form_input($password_confirm);?>
    </p>

    <p>
        <?php echo lang('create_user_photo_label', 'photo');?> <br />
        <?php echo form_input($photo);?>
      </p>


    <p><?php echo form_submit('submit', lang('create_user_submit_btn'),'class="button"');?></p>

    <?php echo form_close();?>

    <!-- __________________________________________________ Finish Content -->

  </div>
</section>
<!-- __________________________________________________ Finish Middle -->




</div>



