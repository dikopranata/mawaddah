<div class="col-sm-3"></div>
<div class="col-sm-6">
  
  <div class="form-box">
    <div class="form-top">
      <div class="form-top-left">
        <h3><?php echo lang('login_heading');?></h3>
        <p><?php echo lang('login_subheading');?></p>
      </div>
      <div class="form-top-right">
        <i class="fa fa-key"></i>
      </div>
    </div>
    <div class="form-bottom">
      <?php if(isset($message)) : ?>
        <div id="infoMessage" class="alert alert-danger"><?php echo $message;?></div>
      <?php endif ?>
      <?php echo form_open("auth/login",array('role' => 'form-action','class' => "login-form"));?>

      <div class="form-group">
        <?php echo lang('login_identity_label', 'identity',array('class' => 'sr-only','for' => "form-username"));?>

        <?php echo form_input(array_merge($identity,array('class'=>'form-username form-control','id' => 'form-username','placeholder'=>'Username...')));?>


      </div>
      <div class="form-group">
        <?php echo lang('login_password_label', 'password',array('class' => 'sr-only','for' => "form-password"));?>
        <?php echo form_input(array_merge($password,array(
          'class'=>'form-password form-control','id' => 'form-password','placeholder'=>'Password...'
          )));?>
        </div>

        <div class="form-group">
         <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
         <?php echo lang('login_remember_label', 'remember');?>

       </div>

       
       <button type="submit" class="btn"><?php echo lang('login_submit_btn')?>!</button>

       <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
     </form>
   </div>
 </div>

</div>
<div class="col-sm-3"></div>





